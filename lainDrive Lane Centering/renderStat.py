import matplotlib.pyplot as plt
from matplotlib import style

style.use("ggplot")

def create_loss_train_graph():
    contents = open("modelT.log", "r").read().split('\n')

    train_loss = []

    for c in contents:
        name = c
        print(name)
        train_loss.append(float(c))

    plt.plot(train_loss)
    plt.show()

def create_loss_valid_graph():
    contents = open("modelV.log", "r").read().split('\n')

    train_loss = []

    for c in contents:
        name = c
        print(name)
        train_loss.append(float(c))

    plt.plot(train_loss)
    plt.show()

if __name__ == "__main__":
    create_loss_train_graph()
    create_loss_valid_graph()